package BulkUpload;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.tool.BulkLoadHFilesTool;


public class HBaseBulkLoad {

    public static void doBulkLoad(String pathToHFile, String tableName) {
        try {
            Configuration configuration = new Configuration();
            configuration.set("mapreduce.child.java.opts", "-Xmx1g");
            Connection connection = ConnectionFactory.createConnection(configuration);
            BulkLoadHFilesTool loadHFiles = new BulkLoadHFilesTool(configuration);
            Table hTable = connection.getTable(TableName.valueOf(tableName));
            Admin admin = connection.getAdmin();
            RegionLocator region = connection.getRegionLocator(TableName.valueOf(tableName));
            loadHFiles.doBulkLoad(new Path(pathToHFile), admin, hTable, region);
            System.out.println("Bulk Load Completed..");
        } catch(Exception exception) {
            exception.printStackTrace();
        }
    }
}
