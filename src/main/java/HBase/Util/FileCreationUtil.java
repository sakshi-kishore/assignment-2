package HBase.Util;

import HBase.Bean.Person;
import com.opencsv.CSVWriter;

import java.io.*;
import java.util.List;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.conf.Configuration;
import java.net.URI;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;

import java.io.OutputStreamWriter;

public class FileCreationUtil {
    public void writeBeanToCSV( List<Person> personList) {
        String uri = "hdfs://localhost:9000";
        String hdfsDir = "hdfs://localhost:9000/capstone/People_Info.csv";
        try {
            Configuration configuration = HBaseConfiguration.create();
            FileSystem hdfs = FileSystem.get(new URI(uri), configuration);
            Path file = new Path(hdfsDir);
            if (hdfs.exists(file)) {
                hdfs.delete(file, true);
            }
            OutputStream os = hdfs.create(file,
                    () -> System.out.println("...bytes written: [ ]"));

            CSVWriter csvWriter = new CSVWriter(new OutputStreamWriter(os, "UTF-8"));
            for (Person p : personList) {
                csvWriter.writeNext(p.toArray());
            }
            csvWriter.close();
            hdfs.close();
            os.close();
        }
        catch (IOException e) {
            System.out.println("IO Exception \n" + e);
        }
        catch (Exception e) {
            System.out.println(e + " occurred.");
        }
    }
}

