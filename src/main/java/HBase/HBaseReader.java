package HBase;

import HBase.Bean.Person;
import HBase.Util.FileCreationUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class HBaseReader {
    public static void main(String[] args) throws IOException {

        List<Person> people = new LinkedList<>();

        Configuration conf = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(conf);
        Table hTable = connection.getTable(TableName.valueOf("People_Info"));
        Scan scan = new Scan();
        ResultScanner scanner = hTable.getScanner(scan);
        Result r;
        while (((r = scanner.next()) != null)) {
            byte[] key = r.getRow();
            byte[] col_family = Bytes.toBytes("Name");
            byte[] name = r.getValue(col_family, Bytes.toBytes("Name"));
            byte[] age = r.getValue(col_family, Bytes.toBytes("Age"));
            byte[] company = r.getValue(col_family, Bytes.toBytes("Company"));
            byte[] buildingCode = r.getValue(col_family, Bytes.toBytes("Building_code"));
            byte[] phoneNumber = r.getValue(col_family, Bytes.toBytes("Phone_Number"));
            byte[] address = r.getValue(col_family, Bytes.toBytes("Address"));

            people.add(new Person(Bytes.toString(key), Bytes.toString(name), Bytes.toString(age),
                    Bytes.toString(company), Bytes.toString(buildingCode),
                    Bytes.toString(phoneNumber), Bytes.toString(address)));
        }
        scanner.close();
        hTable.close();
        new FileCreationUtil().writeBeanToCSV(people);

    }
}

