<b><h1><u>Word Count</u></h1></b><br><br>
<b>WordCountDriver.java</b>

args[0] -> Path of input csv files directory only
args[1] -> Path of output directory, frequency of each word is saved in this directory
Driver of MapReduce job. contains main method

<b>WordCountMapper.java</b>

Mapper class of word count mapreduce job. split csv to individual word and collects each word frequency output - Text, Iterator(IntWritable) (Array of ones).

<b>WordCountReducer.java</b>

Reducer class of word count job input - Text and iterator(IntWritable) output - writes to file the word and size of iterator against each word.


<u><b><h1>package to generate csv file</h1></b></u><br><br>

<b>HBaseReader.java</b>

Parses data from existing HBase table into List of Objects

Writes the data into a csv file and save it on HDFS

<b>Person.java</b>

Bean class to save data from HBase

<u><b><h1>package to generate hfile and populate data to new table</h1></b></u><br><br>

<b>HBaseBulkLoadDriver.java</b>

Driver class of MapReduce job to create hfile from CSV.

Calls HBaseBulkLoad method after successful execution of job

<b>HBaseBulkLoadMapper.java</b>

Mapper class for creating hfile

map function - return Put object for new table and column family

fetched values from CSV (Text input)

<b>HBaseBulkLoad.java</b>

Populate table from hfile created by the mapreduce job.

input - path of HFile